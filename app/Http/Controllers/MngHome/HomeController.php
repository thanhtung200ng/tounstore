<?php


namespace App\Http\Controllers\MngHome;

use App\Http\Controllers\BackendController;
use App\Elibs\eView;



class HomeController extends BackendController
{
    public function __construct()
    {
        parent::__construct('', __DIR__);
       
    }

   public function _list($request)
   {
    return eView::getInstance()->setView(__DIR__, 'index', []);
   }

}
