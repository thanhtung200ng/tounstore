<?php


namespace App\Http\Controllers;

use App\Elibs\eView;
use Illuminate\Http\Request;

/**
 * @property int|mixed editID
 */
class BackendController extends Controller{

    public $model;
    protected $dynamic_request_object = null;
    private $valid; // validate
    protected $dir = ''; 

    function __construct($model = false, $dir= false, $valid = false)
    {
        if(!empty($valid)){
            $this->valid = $valid;
        } //validate
        if(!empty($model)){
            $this->model = $model;
        }
        if(!empty($dir)){
            $this->dir = $dir; // đường dẫn thư mục views
        }
    }

    
    public function index(Request $request, $action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action($request);
        } else {
            return $this->_list($request);
        }
    }
    
    function _list($request) {
    }
    
    public function returnView($blade, $dataExt = [], $breadcrumb = '', $folder = ''){
        return eView::getInstance()->setView($this->dir, $blade, $dataExt);
    }
}