<?php
namespace App\Elibs;

class HtmlHelper
{
    private static $instance = FALSE;
    private $seoMeta = array('title' => '',
        'des' => '',
        'keywords' => '',
        'image' => '',
        'images' => [],
        'robots' => 'INDEX,FOLLOW,ARCHIVE',);
    static $clientVersion = '8686';
    protected static $breadcrumb = [];

    public function __construct()
    {
        //self::$clientVersion = rand(1, 100000);
        self::$instance =& $this;
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            new self();
        }

        return self::$instance;
    }

    function setCssLink($link, $id = '')
    {
        return '<link href="' . url($link) . '?v=' . static::$clientVersion . '" rel="stylesheet" '.'id="'.@$id.'">';
    }

    function setLinkJs($link)
    {
        return '<script type="text/javascript" src="' . url($link) . '?v=' . static::$clientVersion . '"></script>';
    }

    function setLinkJsAsync($link)
    {
        return '<script type="text/javascript" async src="' . url($link) . '?v=' . static::$clientVersion . '"></script>';
    }

}