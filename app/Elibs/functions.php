<?php
if (! function_exists('old_blade')) {

    function old_blade($key = null, $default = null)
    {
        $default = empty($default) ? config('helper.data_edit') : $default;
//        dd($default);
        return app('request')->old($key, optional($default)->$key);
    }
} 
function admin_link($router = '',$withoutProject = FALSE)
{
    return url(str_replace('//', '/', '/' . $router));
}
function css_link($route, $id = false) {
    return \App\Elibs\HtmlHelper::getInstance()->setCssLink($route, $id);
}

function js_link($route) {
    return \App\Elibs\HtmlHelper::getInstance()->setLinkJs($route);
}
