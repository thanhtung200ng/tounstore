<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="_token" content="{{ csrf_token() }}">

    <title>TounStore</title>

    @yield('CSS_REGION')

  <!-- BEGIN: Vendor CSS-->
  {!! css_link('Toun/app-assets/vendors/css/vendors.min.css') !!}
  <!-- END: Vendor CSS-->

  <!-- BEGIN: Theme CSS-->
  {!! css_link('Toun/app-assets/css/bootstrap.min.css') !!}
  {!! css_link('Toun/app-assets/css/bootstrap-extended.min.css') !!}
  {!! css_link('Toun/app-assets/css/colors.min.css') !!}
  {!! css_link('Toun/app-assets/css/components.min.css') !!}
  <!-- END: Theme CSS-->

  <!-- BEGIN: Page CSS-->
  {!! css_link('Toun/app-assets/css/core/menu/menu-types/vertical-menu-modern.css') !!}
  {!! css_link('Toun/app-assets/css/core/colors/palette-gradient.min.css') !!}
  {!! css_link('Toun/app-assets/fonts/simple-line-icons/style.min.css') !!}
  {!! css_link('Toun/app-assets/css/pages/card-statistics.min.css') !!}
  {!! css_link('Toun/app-assets/css/pages/vertical-timeline.min.css') !!}
  <!-- END: Page CSS-->

  <!-- BEGIN: Custom CSS-->
  {!! css_link('Toun/app-assets/css/toun.css') !!}
  <!-- END: Custom CSS-->
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">  
  @livewireStyles
        <script src="{{ mix('js/app.js') }}" defer></script>




</head>

<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" >
    <!-- BEGIN: header-->
  @include('layouts.header')

      <!-- BEGIN: menu-->
  @include('layouts.menu')
      <!-- BEGIN: content-->
  <div class="conmta">
    @yield('CONTENT_REGION')
  </div>
 


  @yield('JS_TOP_REGION')
  <!-- BEGIN: Vendor JS-->
  {!! js_link('Toun/app-assets/vendors/js/vendors.min.js') !!}
  <!-- BEGIN Vendor JS-->
  
  <!-- BEGIN: Page Vendor JS-->
  {!! js_link('Toun/app-assets/vendors/js/charts/apexcharts/apexcharts.min.js') !!}
  <!-- END: Page Vendor JS-->
  
  <!-- BEGIN: Theme JS-->
  {!! js_link('Toun/app-assets/js/core/app-menu.min.js') !!}
  {!! js_link('Toun/app-assets/js/core/app.min.js') !!}
  {!! js_link('Toun/app-assets/js/scripts/customizer.min.js') !!}
  <!-- END: Theme JS-->
  
  <!-- BEGIN: Page JS-->
  {!! js_link('Toun/app-assets/js/scripts/cards/card-statistics.min.js') !!}
  <!-- END: Page JS-->
  @livewireScripts
</body>

</html>
