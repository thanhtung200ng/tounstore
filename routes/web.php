<?php


use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MngHome\HomeController;
use \App\Http\Controllers\MngMember\MemberController;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/45', function () {
    return view('welcome');
});



Route::group(['middleware' => ['admin'], 'prefix' => '/', 'as' => 'Toun.'], function () {
   
    Route::get('/', [HomeController::class, 'index'])->name('Home');
    Route::get('/member/{actions?}', [MemberController::class, 'index'])->name('Member');

});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
